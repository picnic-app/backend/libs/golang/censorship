package censorship_test

import (
	"context"
	"fmt"
	"testing"

	"github.com/golang/mock/gomock"
	"github.com/stretchr/testify/assert"

	"gitlab.com/picnic-app/backend/libs/golang/censorship"
	"gitlab.com/picnic-app/backend/libs/golang/logger"
	adminPb "gitlab.com/picnic-app/backend/libs/golang/protobuf-registry/gen/admin-api/admin/v1"
	adminMock "gitlab.com/picnic-app/backend/libs/golang/protobuf-registry/gen/admin-api/admin/v1/mockgen"
	circlePb "gitlab.com/picnic-app/backend/libs/golang/protobuf-registry/gen/circle-api/circle/v1"
	circleMock "gitlab.com/picnic-app/backend/libs/golang/protobuf-registry/gen/circle-api/circle/v1/mockgen"
)

type reporter struct{}

func (r reporter) Errorf(format string, args ...interface{}) {
	logger.Errorf(context.Background(), format, args...)
}

func (r reporter) Fatalf(format string, args ...interface{}) {
	logger.Fatalf(context.Background(), format, args...)
}

const circleID = "circle id"

var (
	ctx          = context.Background()
	ctrl         = gomock.NewController(reporter{})
	globalClient = adminMock.NewMockAdminAPIClient(ctrl)
	circleClient = circleMock.NewMockCircleAPIClient(ctrl)
)

func init() {
	globalClient.EXPECT().
		GetBlockedURLs(gomock.Any(), gomock.Any()).
		Return(&adminPb.GetBlockedURLsResponse{Urls: []string{"blocked-url.com", "banned-domain.com"}}, nil).
		AnyTimes()

	globalClient.EXPECT().
		GetCensoredWords(gomock.Any(), gomock.Any()).
		Return(&adminPb.GetCensoredWordsResponse{Words: []string{"censored", "censored-word"}}, nil).
		AnyTimes()

	circleClient.EXPECT().
		GetCensoredWords(gomock.Any(), gomock.Any()).
		Return(&circlePb.GetCensoredWordsResponse{Words: []string{"censored-in-circle-word"}}, nil).
		AnyTimes()
}

func ExampleClient_CensorText() {
	cli := censorship.NewClient(globalClient, circleClient)

	censoredText := cli.CensorText(ctx, "Using CENSORED-WORD in text")
	fmt.Println(censoredText)

	// Output:
	// Using ************* in text
}

func ExampleClient_CensorTextInCircle() {
	cli := censorship.NewClient(globalClient, circleClient)

	censoredText := cli.CensorTextInCircle(ctx, "Using CENSORED-IN-CIRCLE-WORD in text", circleID)
	fmt.Println(censoredText)

	// Output:
	// Using *********************** in text
}

func ExampleClient_HasForbiddenWords() {
	cli := censorship.NewClient(globalClient, circleClient)

	found := cli.HasForbiddenWords(ctx, "Using CENSORED-WORD in text")
	fmt.Println(found)

	// Output:
	// true
}

func ExampleClient_HasForbiddenWordsInCircle() {
	cli := censorship.NewClient(globalClient, circleClient)

	found := cli.HasForbiddenWordsInCircle(ctx, "Using CENSORED-WORD in text", circleID)
	fmt.Println(found)

	// Output:
	// true
}

func ExampleClient_IsBlockedURL() {
	cli := censorship.NewClient(globalClient, circleClient)

	blocked := cli.IsBlockedURL(ctx, "WWW.BLOCKED-URL.COM")
	fmt.Println(blocked)

	// Output:
	// true
}

func ExampleClient_ContainsBlockedURL() {
	cli := censorship.NewClient(globalClient, circleClient)

	blocked := cli.ContainsBlockedURL(ctx, "Using WWW.BLOCKED-URL.COM in text")
	fmt.Println(blocked)

	// Output:
	// true
}

func TestContainsBlockedURL(t *testing.T) {
	cli := censorship.NewClient(globalClient, circleClient)

	assert.Equal(t, true, cli.ContainsBlockedURL(
		ctx, "Using full domain https://www.banned-domain.com in text"))

	assert.Equal(t, true, cli.ContainsBlockedURL(
		ctx, "Using full domain http://www.banned-domain.com in text"))

	assert.Equal(t, true, cli.ContainsBlockedURL(
		ctx, "Using shorten domain BANNED-domain.com in text"))

	assert.Equal(t, false, cli.ContainsBlockedURL(
		ctx, "Using shorten domain not-BANNED-domain.com in text"))
}
