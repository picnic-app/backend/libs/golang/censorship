package censorship

import (
	"context"
	"regexp"
	"sort"
	"strings"
	"sync"
	"unsafe"

	"go.uber.org/zap"
	"google.golang.org/grpc"

	"gitlab.com/picnic-app/backend/libs/golang/logger"
	adminPb "gitlab.com/picnic-app/backend/libs/golang/protobuf-registry/gen/admin-api/admin/v1"
	circlePb "gitlab.com/picnic-app/backend/libs/golang/protobuf-registry/gen/circle-api/circle/v1"
)

var urlsRegex = regexp.MustCompile(`\b(?:https?://(?:[a-zA-Z0-9-]+\.)+[a-zA-Z]{2,}(?:[/?&]\S*)?|(www\.)?[a-zA-Z0-9-]+\.[a-zA-Z]{2,}(?:[/?&]\S*)?)\b`)
var domainsRegex = regexp.MustCompile(`(?:https?:)?(?:\/\/)?(?:[^@\n]+@)?(?:www\.)?([^:\/\n]+)`)

type CircleCensorClient interface {
	GetCensoredWords(
		context.Context,
		*circlePb.GetCensoredWordsRequest,
		...grpc.CallOption,
	) (*circlePb.GetCensoredWordsResponse, error)
}

type GlobalCensorClient interface {
	GetCensoredWords(
		context.Context,
		*adminPb.GetCensoredWordsRequest,
		...grpc.CallOption,
	) (*adminPb.GetCensoredWordsResponse, error)
	GetBlockedURLs(
		context.Context,
		*adminPb.GetBlockedURLsRequest,
		...grpc.CallOption,
	) (*adminPb.GetBlockedURLsResponse, error)
}

func NewClient(
	global GlobalCensorClient,
	circle CircleCensorClient,
) Client {
	return Client{
		circle: circle,
		global: global,
	}
}

type Client struct {
	circle CircleCensorClient
	global GlobalCensorClient
}

func (c Client) IsBlockedURL(ctx context.Context, url string) bool {
	resp, err := c.global.GetBlockedURLs(ctx, &adminPb.GetBlockedURLsRequest{})
	if err != nil {
		logger.ErrorKV(ctx, "getting blocked URLs", zap.Error(err))
		return false
	}

	url = strings.ToLower(url)
	domains := domainsRegex.FindStringSubmatch(url)
	domain := domains[1]

	for _, blocked := range resp.GetUrls() {
		if domain == blocked {
			return true
		}
	}

	return false
}

func (c Client) ContainsBlockedURL(ctx context.Context, text string) bool {
	resp, err := c.global.GetBlockedURLs(ctx, &adminPb.GetBlockedURLsRequest{})
	if err != nil {
		logger.ErrorKV(ctx, "getting blocked URLs", zap.Error(err))
		return false
	}

	// Find all matches in the url
	urls := urlsRegex.FindAllString(strings.ToLower(text), -1)

	for _, url := range urls {
		domains := domainsRegex.FindStringSubmatch(url)
		// index 0 will have the full domain like `https://www.google.com`
		// index 1 will have the shorten domain like `google.com`
		domain := domains[1]
		for _, blocked := range resp.GetUrls() {
			if blocked == domain {
				return true
			}
		}
	}
	return false
}

func (c Client) HasForbiddenWords(ctx context.Context, text string) bool {
	return c.HasForbiddenWordsInCircle(ctx, text, "")
}

func (c Client) HasForbiddenWordsInCircle(ctx context.Context, text, circleID string) bool {
	text = strings.ToLower(text)
	for _, word := range c.getBlockedTexts(ctx, circleID) {
		if strings.Contains(text, word) {
			return true
		}
	}

	return false
}

func (c Client) CensorText(ctx context.Context, text string) string {
	return c.CensorTextInCircle(ctx, text, "")
}

func (c Client) CensorTextInCircle(ctx context.Context, text, circleID string) string {
	words := c.getBlockedTexts(ctx, circleID)
	if len(words) == 0 {
		return text
	}

	sort.Slice(words, func(i, j int) bool { return len(words[i]) > len(words[j]) })

	buf := []byte(text)
	text = strings.ToLower(text)
	asterisks := strings.Repeat("*", len(words[0]))

	for _, word := range words {
		i := strings.Index(text, word)
		for i >= 0 && len(word) > 0 {
			i += copy(buf[i:i+len(word)], asterisks)
			i = strings.Index(text[i:], word)
		}
	}

	return *(*string)(unsafe.Pointer(&buf)) //nolint:gosec // This is a safe operation.
}

func (c Client) getBlockedTexts(ctx context.Context, circleID string) []string {
	var wg sync.WaitGroup

	var circleWords, globalWords, urls []string
	wg.Add(2)
	go func() {
		defer wg.Done()

		resp, err := c.global.GetBlockedURLs(ctx, &adminPb.GetBlockedURLsRequest{})
		if err != nil {
			logger.ErrorKV(ctx, "getting global blocked URLs", zap.Error(err))
		}

		urls = resp.GetUrls()
	}()

	go func() {
		defer wg.Done()

		resp, err := c.global.GetCensoredWords(ctx, &adminPb.GetCensoredWordsRequest{})
		if err != nil {
			logger.ErrorKV(ctx, "getting global censored words", zap.Error(err))
		}

		globalWords = resp.GetWords()
	}()

	if circleID != "" && c.circle != nil {
		wg.Add(1)
		go func() {
			defer wg.Done()

			resp, err := c.circle.GetCensoredWords(ctx, &circlePb.GetCensoredWordsRequest{CircleId: circleID})
			if err != nil {
				logger.ErrorKV(ctx, "getting circle blocked words", zap.Error(err))
			}

			circleWords = resp.GetWords()
		}()
	}

	wg.Wait()

	return append(append(globalWords, urls...), circleWords...)
}
